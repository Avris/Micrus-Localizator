<?php
namespace Avris\Micrus\Localizator;

class LocalizatorTwigTest extends \PHPUnit_Framework_TestCase
{
    public function testTwig()
    {
        $localizator = $this->getMockBuilder(Localizator::class)->disableOriginalConstructor()->getMock();
        $localizator->expects($this->once())->method('get')->willReturnCallback(function ($word, $replacements = []) {
            return $word === 'foo.bar'
                ? strtr('OK %value%', $replacements)
                : 'NOT FOUND';
        });

        $twig = new LocalizatorTwig($localizator);
        $filters = $twig->getFilters();
        $this->assertCount(1, $filters);
        /** @var \Twig_SimpleFilter $filter */
        $filter = $filters[0];
        $this->assertInstanceOf(\Twig_SimpleFilter::class, $filter);
        $this->assertSame('l', $filter->getName());

        $translated = call_user_func($filter->getCallable(), 'foo.bar', ['%value%' => 'YES']);
        $this->assertEquals('OK YES', $translated);
    }
}
