<?php
namespace Avris\Micrus\Localizator;

use Avris\Micrus\Localizator\Order\TranslationOrder;

class LocalizatorAppVarsTest extends \PHPUnit_Framework_TestCase
{
    public function testVars()
    {
        $localizator = $this->getMockBuilder(Localizator::class)->disableOriginalConstructor()->getMock();
        $localizator->expects($this->once())->method('getSupportedLocales')->willReturn(['de' => 'Deutsch']);

        $order = $this->getMockBuilder(TranslationOrder::class)->disableOriginalConstructor()->getMock();
        $order->expects($this->once())->method('main')->willReturn('de');

        $appVarsBuilder = new LocalizatorAppVars($localizator, $order);
        $this->assertSame([
            'locales' => ['de' => 'Deutsch'],
            'locale' => 'de',
        ], $appVarsBuilder->buildAppVars());
    }
}
