<?php
namespace Avris\Micrus\Localizator\Locale;

class LocaleTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider dataProvider
     */
    public function testData($string, $language, $country, $isGeneral, $flag)
    {
        $locale = new Locale($string);
        $this->assertSame($string, (string) $locale);
        $this->assertSame($language, $locale->getLanguage());
        $this->assertSame($country, $locale->getCountry());
        $this->assertSame($isGeneral, $locale->isGeneral());
        $this->assertSame($flag, $locale->guessFlag());
    }

    public function dataProvider()
    {
        return [
            ['de', 'de', null, true, 'DE'],
            ['de_AT', 'de', 'AT', false, 'AT'],
        ];
    }
}
