<?php
namespace Avris\Micrus\Localizator\Locale;

use Avris\Bag\Bag;

class LocaleSetTest extends \PHPUnit_Framework_TestCase
{
    public function testPhp()
    {
        $set = new PhpLocaleSet('test', __DIR__ . '/../_help/LocalePHP');
        $this->assertSame('OK', $set->get('en', 'foo.bar'));
        $this->assertNull($set->get('en', 'nope'));
        $this->assertNull($set->get('pl', 'foo.bar'));
    }

    public function testYaml()
    {
        $set = new YamlLocaleSet('test', __DIR__ . '/../_help/LocaleYAML');
        $this->assertSame('OK', $set->get('en', 'foo.bar'));
        $this->assertNull($set->get('en', 'nope'));
        $this->assertNull($set->get('ps', 'foo.bar'));
    }

    public function testLocaleList()
    {
        $set = new YamlLocaleSet('test', __DIR__ . '/../_help/LocaleYAML');

        $locales = $set->all();
        $this->assertInstanceOf(Bag::class, $locales);
        $this->assertInstanceOf(FileLocale::class, $locales['en']);
        $this->assertSame(['en', 'pl'], $locales->keys());
    }

    /**
     * @dataProvider fallbackProvider
     */
    public function testFallback($defined, $expected)
    {
        $set = new YamlLocaleSet('test', __DIR__ . '/../_help/LocaleYAML', $defined);
        $this->assertInstanceOf(LocaleInterface::class, $set->getFallback());
        $this->assertSame($expected, (string) $set->getFallback());
    }

    public function fallbackProvider()
    {
        return [
            ['en', 'en'],       // exists
            ['pl_PL', 'pl'],    // parent exists
            ['ps', 'ps'],       // does not exist
            ['ps_AF', 'ps_AF'], // does not exist
        ];
    }

    public function testName()
    {
        $set = new YamlLocaleSet('test', __DIR__);
        $this->assertSame('test', $set->getName());
    }
}
