<?php
namespace Avris\Micrus\Localizator\Locale;

use Avris\Bag\Bag;

class FileLocaleTest extends \PHPUnit_Framework_TestCase
{
    public function testAll()
    {
        $set = $this->getMockBuilder(FileLocaleSet::class)->disableOriginalConstructor()->getMock();
        $set->expects($this->once())->method('loadDataForLocale')->willReturn(new Bag([
            'foo' => [
                'bar' => 'OK',
            ],
        ]));

        $locale = new FileLocale($set, 'de_DE');

        $this->assertSame([
            'foo' => [
                'bar' => 'OK',
            ],
        ], $locale->all());

        $this->assertSame('OK', $locale->get('foo.bar'));

        $this->assertSame([
            1 => 'data',
            2 => 'language',
            3 => 'country',
        ], $locale->__sleep());
    }
}
