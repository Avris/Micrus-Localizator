<?php
namespace Avris\Micrus\Localizator\Handler;

use Avris\Micrus\Bootstrap\ContainerInterface;
use Avris\Micrus\Controller\Http\RedirectResponse;
use Avris\Micrus\Controller\Http\RequestInterface;
use Avris\Micrus\Controller\Http\SessionBag;
use Avris\Micrus\Controller\Routing\Event\AddRouteEvent;
use Avris\Micrus\Controller\Routing\Event\GenerateRouteEvent;
use Avris\Micrus\Controller\Routing\Model\CompiledRoute;
use Avris\Micrus\Controller\Routing\Model\Route;
use Avris\Micrus\Localizator\Localizator;
use Avris\Micrus\Localizator\Order\TranslationOrder;

class UrlLocaleHandlerTest extends \PHPUnit_Framework_TestCase
{
    /** @var ContainerInterface|\PHPUnit_Framework_MockObject_MockObject */
    protected $container;

    /** @var UrlLocaleHandler */
    protected $handler;

    protected function setUp()
    {
        $this->container = $this->getMockBuilder(ContainerInterface::class)->disableOriginalConstructor()->getMock();

        $this->handler = new UrlLocaleHandler($this->container);
        $this->assertSame(['_locale' => null], $this->handler->getSpecialTags());
    }

    public function testAddRoute()
    {
        $route = new Route('test', '/test', '@test/test');
        $event = new AddRouteEvent('test', $route);

        $this->handler->onAddRoute($event);

        $this->assertSame('/{_locale}/test', $event->getRoute()->getPattern());
        $this->assertSame(['_locale' => ''], $event->getRoute()->getDefaults());
    }

    public function testAddRouteImmutable()
    {
        $route = new Route('test', '/test', '@test/test', [], [], [], null, [], ['immutable' => true]);
        $event = new AddRouteEvent('test', $route);

        $this->handler->onAddRoute($event);

        $this->assertSame('/test', $event->getRoute()->getPattern());
        $this->assertSame([], $event->getRoute()->getDefaults());
    }

    public function testGenerateRoute()
    {
        $order = $this->getMockBuilder(TranslationOrder::class)->disableOriginalConstructor()->getMock();
        $order->expects($this->once())->method('first')->willReturn('it');
        $this->container->expects($this->once())->method('get')->with('translationOrder')->willReturn($order);

        $route = new CompiledRoute(
            new Route('test', '/{_locale}/test', '@test/test', ['_locale' => 'en']),
            '',
            ['_locale']
        );
        $event = new GenerateRouteEvent($route);

        $this->handler->onGenerateRoute($event);

        $this->assertSame(['_locale' => 'it'], $event->getParams());
    }

    public function testGenerateRouteNoTag()
    {
        $this->container->expects($this->never())->method('get');

        $route = new CompiledRoute(new Route('test', '/test', '@test/test'), '', []);
        $event = new GenerateRouteEvent($route);

        $this->handler->onGenerateRoute($event);

        $this->assertSame([], $event->getParams());
    }

    public function testGenerateRouteAlreadyGiven()
    {
        $this->container->expects($this->never())->method('get');

        $route = new CompiledRoute(
            new Route('test', '/{_locale}/test', '@test/test', ['_locale' => 'en']),
            '',
            ['_locale']
        );
        $event = new GenerateRouteEvent($route, ['_locale' => 'de']);

        $this->handler->onGenerateRoute($event);

        $this->assertSame(['_locale' => 'de'], $event->getParams());
    }
}
