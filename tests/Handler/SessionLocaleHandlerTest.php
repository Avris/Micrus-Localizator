<?php
namespace Avris\Micrus\Localizator\Handler;

use Avris\Micrus\Bootstrap\ContainerInterface;
use Avris\Micrus\Bootstrap\EventDispatcherInterface;
use Avris\Micrus\Controller\Http\RedirectResponse;
use Avris\Micrus\Controller\Http\RequestInterface;
use Avris\Micrus\Controller\Http\SessionBag;
use Avris\Micrus\Localizator\Localizator;

class SessionLocaleHandlerTest extends \PHPUnit_Framework_TestCase
{
    /** @var Localizator|\PHPUnit_Framework_MockObject_MockObject */
    protected $localizator;

    /** @var EventDispatcherInterface|\PHPUnit_Framework_MockObject_MockObject */
    protected $dispatcher;

    /** @var ContainerInterface|\PHPUnit_Framework_MockObject_MockObject */
    protected $container;

    protected function setUp()
    {
        $this->localizator = $this->getMockBuilder(Localizator::class)->disableOriginalConstructor()->getMock();
        $this->localizator->expects($this->any())->method('getSupportedLocales')->willReturn([
            'en' => 'English',
        ]);
        $this->dispatcher = $this->getMockBuilder(EventDispatcherInterface::class)
            ->disableOriginalConstructor()->getMock();

        $this->container = $this->getMockBuilder(ContainerInterface::class)->disableOriginalConstructor()->getMock();
        $this->localizator->expects($this->any())->method('trigger');
    }

    public function testRoute()
    {
        $handler = new SessionLocaleHandler($this->localizator, $this->dispatcher);
        $this->assertArrayHasKey('changeLocale', $handler->getRouting());
    }

    /**
     * @expectedException \Avris\Micrus\Exception\NotFoundException
     */
    public function testChangeLocaleNotSupported()
    {
        $handler = new SessionLocaleHandler($this->localizator, $this->dispatcher);
        $handler->changeLocaleAction($this->container, 'ps');
    }

    public function testChangeLocaleSuccess()
    {
        $session = $this->getMockBuilder(SessionBag::class)->disableOriginalConstructor()->getMock();
        $session->expects($this->once())->method('set')->with('_locale', 'en');

        $request = $this->getMockBuilder(RequestInterface::class)->disableOriginalConstructor()->getMock();
        $request->expects($this->exactly(2))->method('getSession')->willReturn($session);
        $request->expects($this->once())->method('getHeaders')->with('referer')->willReturn('/ok');

        $this->container->expects($this->once())->method('get')->with('request')->willReturn($request);

        $handler = new SessionLocaleHandler($this->localizator, $this->dispatcher);
        $response = $handler->changeLocaleAction($this->container, 'en');

        $this->assertInstanceOf(RedirectResponse::class, $response);
        $this->assertSame('/ok', (string) $response->getHeaders()->get('location'));
    }
}
