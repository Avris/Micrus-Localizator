<?php
namespace Avris\Micrus\Localizator\String;

use Avris\Micrus\Localizator\Localizator;

class LocalizedStringTest extends \PHPUnit_Framework_TestCase
{
    public function testAll()
    {
        require_once __DIR__ . '/../../src/String/function.php';

        $localizator = $this->getMockBuilder(Localizator::class)->disableOriginalConstructor()->getMock();
        $localizator->expects($this->any())->method('get')->willReturnCallback(function ($word, $replacements = []) {
            return $word === 'foo.bar'
                ? strtr('OK %value%', $replacements)
                : 'NOT FOUND';
        });
        $localizator->expects($this->any())->method('has')->willReturnCallback(function ($word, $replacements = []) {
            return $word === 'foo.bar';
        });

        $orgLocalizator = LocalizedString::getLocalizator();
        LocalizedString::setLocalizator($localizator);

        $ls = l('foo.bar', ['%value%' => 'YES']);
        $this->assertInstanceOf(LocalizedString::class, $ls);
        $this->assertSame('OK YES', $ls->getLocalized());
        $this->assertSame('OK YES', (string) $ls);
        $this->assertSame('"OK YES"', json_encode($ls));
        $this->assertTrue($ls->exists());

        $ls = l('nope', ['%value%' => 'YES']);
        $this->assertInstanceOf(LocalizedString::class, $ls);
        $this->assertSame('NOT FOUND', $ls->getLocalized());
        $this->assertSame('NOT FOUND', (string) $ls);
        $this->assertSame('"NOT FOUND"', json_encode($ls));
        $this->assertFalse($ls->exists());

        LocalizedString::setLocalizator(null);

        $ls = l('Value is %value%', ['%value%' => '5']);
        $this->assertInstanceOf(LocalizedString::class, $ls);
        $this->assertSame('Value is 5', $ls->getLocalized());
        $this->assertSame('Value is 5', (string) $ls);
        $this->assertSame('"Value is 5"', json_encode($ls));
        $this->assertTrue($ls->exists());

        LocalizedString::setLocalizator($orgLocalizator);
    }
}
