<?php
namespace Avris\Micrus\Localizator\Selector;

class PolishDeclinationTest extends \PHPUnit_Framework_TestCase
{
    /** @var PolishDeclination */
    protected $selector;

    protected function setUp()
    {
        $this->selector = new PolishDeclination();
        $this->assertSame('polishDeclination', $this->selector->getName());
        $this->assertStringStartsWith('function (replacements, versions)', $this->selector->getJsFunction());
    }

    /**
     * @dataProvider selectProvider
     */
    public function testSelect($count, $expected)
    {
        $this->assertSame(
            $expected,
            $this->selector->select(['%count%' => $count], ['pies', 'psy', 'psów'])
        );
    }

    public function selectProvider()
    {
        return [
            [0, 'psów'],
            [1, 'pies'],
            [2, 'psy'],
            [3, 'psy'],
            [4, 'psy'],
            [5, 'psów'],
            [6, 'psów'],
            [7, 'psów'],
            [8, 'psów'],
            [9, 'psów'],
            [10, 'psów'],
            [11, 'psów'],
            [12, 'psów'],
            [13, 'psów'],
            [14, 'psów'],
            [15, 'psów'],
            [16, 'psów'],
            [17, 'psów'],
            [18, 'psów'],
            [19, 'psów'],
            [20, 'psów'],
            [21, 'psów'],
            [22, 'psy'],
            [23, 'psy'],
            [24, 'psy'],
            [25, 'psów'],
        ];
    }
}
