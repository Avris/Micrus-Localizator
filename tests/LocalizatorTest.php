<?php
namespace Avris\Micrus\Localizator;

use Avris\Bag\Bag;
use Avris\Micrus\Localizator\Locale\FileLocaleSet;
use Avris\Micrus\Localizator\Locale\Locale;
use Avris\Micrus\Localizator\Order\TranslationOrder;
use Avris\Micrus\Localizator\Selector\TranslationSelector;
use Avris\Micrus\Localizator\String\LocalizedString;

class LocalizatorTest extends \PHPUnit_Framework_TestCase
{
    /** @var Localizator */
    protected $localizator;

    protected function setUp()
    {
        $lsA = $this->getMockBuilder(FileLocaleSet::class)->disableOriginalConstructor()->getMock();
        $lsA->expects($this->any())->method('getName')->willReturn('A');
        $lsA->expects($this->any())->method('all')->willReturn(['en' => 'ALL(A-EN)']);
        $lsA->expects($this->any())->method('get')->willReturnCallback(function ($locale, $word) {
            switch ($locale) {
                case 'en':
                    switch ($word) {
                        case 'aaa': return 'enAAA';
                        case 'bbb': return 'enBBB';

                        case 'replacements': return 'Key is %key% and value is %value%';
                        case 'double':       return 'foo [[tripple]]';
                        case 'tripple':      return 'bar';
                        case 'selector':     return '<SEL> one %nr%|two %nr%|three %nr%';
                        default:    return null;
                    }
                default: return null;
            }
        });

        $lsB = $this->getMockBuilder(FileLocaleSet::class)->disableOriginalConstructor()->getMock();
        $lsB->expects($this->any())->method('getName')->willReturn('B');
        $lsB->expects($this->any())->method('all')->willReturn(['de' => 'ALL(B-DE)', 'en' => 'ALL(B-EN)']);
        $lsB->expects($this->any())->method('get')->willReturnCallback(function ($locale, $word) {
            switch ($locale) {
                case 'de':
                    switch ($word) {
                        case 'aaa': return 'deXXX';
                        default:    return null;
                    }
                case 'en':
                    switch ($word) {
                        case 'aaa': return 'enXXX';
                        case 'bbb': return 'enYYY';
                        default:    return null;
                    }
                default: return null;
            }
        });

        $supported = new Bag(['de' => 'Deutsch', 'en' => 'Englisch']);

        $selector = $this->getMockBuilder(TranslationSelector::class)->disableOriginalConstructor()->getMock();
        $selector->expects($this->any())->method('getName')->willReturn('SEL');
        $selector->expects($this->any())->method('select')->willReturnCallback(function ($replacements, $versions) {
            return $versions[$replacements['%nr%']];
        });

        $this->localizator = new Localizator(
            [$lsA, $lsB],
            new TranslationOrder($supported, [new Locale('de'), new Locale('en')]),
            $supported,
            [$selector],
            ['^pattern\.(.*)\.test$']
        );
    }

    public function testGetEmpty()
    {
        $this->assertSame('', $this->localizator->get('', ['foo' => 'bar'], 'A'));
    }

    public function testGetAnySet()
    {
        $this->assertSame('deXXX', $this->localizator->get('aaa'));
        $this->assertSame('enBBB', $this->localizator->get('bbb'));

        $l = $this->localizator;
        $this->assertSame('deXXX', $l('aaa'));
    }

    public function testGetSpecificSet()
    {
        $this->assertSame('enYYY', $this->localizator->get('bbb', [], 'B'));
    }

    public function testGetSpecificLocale()
    {
        $this->assertSame('enAAA', $this->localizator->get('aaa', [], null, 'en_US'));
    }

    public function testGetDefaultPattern()
    {
        $this->assertSame('Ok', $this->localizator->get('pattern.ok.test'));
    }

    public function testGetFallback()
    {
        $this->assertSame('not.found', $this->localizator->get('not.found'));
    }

    public function testGetReplacements()
    {
        $this->assertSame(
            'Key is KEY and value is VALUE',
            $this->localizator->get('replacements', ['key' => 'KEY', '%value%' => 'VALUE'])
        );
    }

    public function testGetDouble()
    {
        $this->assertSame('foo bar', $this->localizator->get('double'));
    }

    public function testGetSelector()
    {
        $this->assertSame('one 0', $this->localizator->get('selector', ['nr' => 0]));
        $this->assertSame('two 1', $this->localizator->get('selector', ['nr' => 1]));
        $this->assertSame('three 2', $this->localizator->get('selector', ['nr' => 2]));
    }

    public function testHas()
    {
        $this->assertTrue($this->localizator->has('bbb'));
        $this->assertFalse($this->localizator->has('ccc'));
    }

    public function testGetters()
    {
        $this->assertSame(
            [
                'de' => 'Deutsch',
                'en' => 'Englisch',
            ],
            $this->localizator->getSupportedLocales()
        );

        $this->assertCount(2, $this->localizator->getLocaleSets());
        $this->assertSame(['A', 'B'], array_keys($this->localizator->getLocaleSets()));
        $this->assertInstanceOf(FileLocaleSet::class, $this->localizator->getLocaleSets()['A']);

        $this->assertSame(['de', 'en'], $this->localizator->getTranslationOrder());

        $this->assertCount(1, $this->localizator->getSelectors());
        $this->assertSame(['SEL'], array_keys($this->localizator->getSelectors()));
        $this->assertInstanceOf(TranslationSelector::class, $this->localizator->getSelectors()['SEL']);

        $this->assertSame(
            ['^pattern\.(.*)\.test$'],
            $this->localizator->getDefaultPatterns()
        );
    }

    public function testAll()
    {
        $this->assertSame(
            [
                'A' => [
                    'en' => 'ALL(A-EN)',
                ],
                'B' => [
                    'de' => 'ALL(B-DE)',
                    'en' => 'ALL(B-EN)',
                ],
            ],
            $this->localizator->all()
        );
    }

    public function testEvents()
    {
        $orgLocalizator = LocalizedString::getLocalizator();
        LocalizedString::setLocalizator(null);

        $this->localizator->onRequest();
        $this->assertSame($this->localizator, LocalizedString::getLocalizator());

        LocalizedString::setLocalizator($orgLocalizator);
    }
}
