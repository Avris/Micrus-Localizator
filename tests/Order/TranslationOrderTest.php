<?php
namespace Avris\Micrus\Localizator\Locale;

use Avris\Bag\Bag;
use Avris\Micrus\Localizator\Order\TranslationOrder;

class TranslationOrderTest extends \PHPUnit_Framework_TestCase
{
    /** @var TranslationOrder  */
    protected $order;

    public function setUp()
    {
        $this->order = new TranslationOrder(new Bag([
            'en' => 'English',
            'en_GB' => 'English (UK)',
            'de_DE' => 'Deutsch (Deutschland)'
        ]));
    }

    public function testAutomaticallyAddParent()
    {
        $this->order->add('en_GB');
        $this->assertSame(['en_GB', 'en'], $this->order->getList());
    }

    public function testAutomaticallyAddParentNotDirectlySupported()
    {
        $this->order->add('de_DE');
        $this->assertSame(['de_DE', 'de'], $this->order->getList());
    }

    public function testAddNotSupported()
    {
        $this->order->add('fr');
        $this->order->add('fr_FR');
        $this->assertSame([], $this->order->getList());
    }

    public function testAddNotSupportedForce()
    {
        $this->order->add('fr_FR', true);
        $this->assertSame(['fr_FR'], $this->order->getList());
    }

    public function testAddEmptyIgnored()
    {
        $this->order->add(null);
        $this->assertSame([], $this->order->getList());
    }

    public function testMainSupported()
    {
        $this->order->add('en');
        $this->order->add('de_DE');
        $this->assertEquals(new Locale('en'), $this->order->main());
    }

    public function testMainNotSupported()
    {
        $this->order->add('fr', true);
        $this->assertEquals(new Locale('fr'), $this->order->main());
    }
}
