<?php
namespace Avris\Micrus\Localizator\Locale;

use Avris\Bag\Bag;
use Avris\Micrus\Controller\Http\HeaderBag;
use Avris\Micrus\Controller\Http\QualityHeader;
use Avris\Micrus\Controller\Http\RequestInterface;
use Avris\Micrus\Controller\Routing\Model\RouteMatch;
use Avris\Micrus\Localizator\Order\TranslationOrder;
use Avris\Micrus\Localizator\Order\TranslationOrderResolver;

class TranslationOrderResolverTest extends \PHPUnit_Framework_TestCase
{
    /** @var RequestInterface|\PHPUnit_Framework_MockObject_MockObject */
    protected $request;

    /** @var HeaderBag|\PHPUnit_Framework_MockObject_MockObject */
    protected $headers;

    protected function setUp()
    {
        $this->headers = $this->getMockBuilder(HeaderBag::class)->disableOriginalConstructor()->getMock();
        $this->request = $this->getMockBuilder(RequestInterface::class)->disableOriginalConstructor()->getMock();
        $this->request->expects($this->once())->method('getHeaders')->willReturn($this->headers);
    }

    public function testResolveEmpty()
    {
        $resolver = new TranslationOrderResolver($this->request, new Bag());
        $order = $resolver->resolve();

        $this->assertInstanceOf(TranslationOrder::class, $order);
        $this->assertSame(['en'], $order->getList());
    }

    public function testResolveFull()
    {
        $this->request->expects($this->once())->method('getSession')->with('_locale')->willReturn('it');

        $routeMatch = $this->getMockBuilder(RouteMatch::class)->disableOriginalConstructor()->getMock();
        $routeMatch->expects($this->exactly(2))->method('getTags')->willReturn(['_locale' => 'de']);
        $this->request->expects($this->once())->method('getRouteMatch')->willReturn($routeMatch);

        $header = $this->getMockBuilder(QualityHeader::class)->disableOriginalConstructor()->getMock();
        $header->expects($this->once())->method('getSorted')->willReturn(['pl', 'en']);
        $this->headers->expects($this->once())->method('getLanguage')->willReturn($header);

        $resolver = new TranslationOrderResolver($this->request, new Bag([
            'en' => 'English',
            'de' => 'Deutsch',
            'it' => 'Italiano',
            'pl' => 'Polski',
            'es' => 'Español',
            'ru' => 'Русский',
        ]), 'es');
        $order = $resolver->resolve();

        $this->assertInstanceOf(TranslationOrder::class, $order);
        $this->assertSame(['it', 'de', 'pl', 'en', 'es'], $order->getList());
    }
}
