<?php
namespace Avris\Micrus\Localizator;

class LocalizatorModuleTest extends \PHPUnit_Framework_TestCase
{
    public function testModule()
    {
        $module = new LocalizatorModule();
        $config = $module->extendConfig('test', __DIR__);
        $this->assertSame(
            [
                'localizator',
                'appLocaleSet',
                'localizatorHandler',
                'translationOrder',
                'countVersion',
                'localizeTwig',
                'localizeAppVars',
            ],
            array_keys($config['services'])
        );
    }
}
