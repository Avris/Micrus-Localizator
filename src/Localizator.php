<?php
namespace Avris\Micrus\Localizator;

use Avris\Bag\Bag;
use Avris\Micrus\Localizator\Locale\LocaleInterface;
use Avris\Micrus\Localizator\Locale\LocaleSetInterface;
use Avris\Micrus\Localizator\Order\TranslationOrder;
use Avris\Micrus\Localizator\Selector\TranslationSelector;
use Avris\Micrus\Localizator\String\LocalizedString;

class Localizator
{
    /** @var LocaleSetInterface[] */
    protected $localeSets;

    /** @var string[] */
    protected $supportedLocales;

    /** @var TranslationOrder */
    protected $translationOrder;

    /** @var TranslationSelector[] */
    protected $selectors = [];

    /** @var string[] */
    protected $defaultPatterns = [];

    /**
     * @param LocaleSetInterface[] $localeSets
     * @param Bag $supportedLocales
     * @param TranslationOrder $translationOrder
     * @param TranslationSelector[] $selectors
     * @param string[] $defaultPatterns
     */
    public function __construct(
        array $localeSets,
        TranslationOrder $translationOrder,
        Bag $supportedLocales = null,
        $selectors = [],
        $defaultPatterns = []
    ) {
        $this->localeSets = [];
        foreach ($localeSets as $localeSet) {
            $this->localeSets[$localeSet->getName()] = $localeSet;
        }

        $this->supportedLocales = $supportedLocales ? $supportedLocales->all() : [];

        $translationOrder = clone $translationOrder;
        foreach ($localeSets as $localeSet) {
            $translationOrder->add((string) $localeSet->getFallback());
        }
        $this->translationOrder = $translationOrder->getList();

        foreach ($selectors as $selector) {
            $this->selectors[$selector->getName()] = $selector;
        }

        $this->defaultPatterns = $defaultPatterns;
    }

    /**
     * @param string $word
     * @param string[] $replacements
     * @param string|null $set which LocaleSet to use (default: all)
     * @param LocaleInterface|string|null $locale which locale to use (default: all in the TranslationOrder)
     * @return string|null
     */
    public function get($word, $replacements = [], $set = null, $locale = null)
    {
        if (!$word) {
            return '';
        }

        $translated = $this->findWord($word, $set, $locale) ?: $this->fallbackToDefaultPatterns($word) ?: $word;

        $replacements = $this->filterReplacements($replacements);

        $translated = preg_replace_callback('#\[\[(.+)\]\]#Ui', function ($matches) use ($replacements, $set, $locale) {
            return $this->get(strtr($matches[1], $replacements), $replacements, $set, $locale);
        }, $translated);

        if (!count($replacements)) {
            return $translated;
        }

        // TODO allow multiple? eg. count+gender
        if (preg_match('#^<([A-Za-z0-9]*)>(.*)$#', $translated, $matches)) {
            list(,$selector,$versions) = $matches;
            if (isset($this->selectors[$selector])) {
                $versions = array_map('trim', explode('|', $versions));

                $translated = $this->selectors[$selector]->select($replacements, $versions);
            }
        }

        return strtr($translated, $replacements);
    }

    /**
     * @param string $word
     * @param string|null $set
     * @param LocaleInterface|string|null $locale
     * @return string|null
     */
    protected function findWord($word, $set = null, $locale = null)
    {
        $checkedSets = $set === null
            ? $this->localeSets
            : (isset($this->localeSets[$set]) ? [$this->localeSets[$set]] : []);

        foreach ($this->translationOrderWithForced($locale) as $locale) {
            foreach ($checkedSets as $checkedSet) {
                $translated = $checkedSet->get($locale, $word);
                if ($translated !== null) {
                    return $translated;
                }
            }
        }

        return null;
    }

    /**
     * @param LocaleInterface|string|null $forcedLocale
     * @return LocaleInterface[]
     */
    protected function translationOrderWithForced($forcedLocale = null)
    {
        if ($forcedLocale === null) {
            return $this->translationOrder;
        }

        $order = [$forcedLocale];
        if (strlen($forcedLocale) == 5) {
            $order[] = substr($forcedLocale, 0, 2);
        }

        return $order;
    }

    protected function fallbackToDefaultPatterns($word)
    {
        foreach ($this->defaultPatterns as $pattern) {
            if (preg_match('#'.$pattern.'#', $word, $matches)) {
                return ucfirst($matches[1]);
            }
        }

        return null;
    }

    protected function filterReplacements(array $replacements)
    {
        $new = [];
        foreach ($replacements as $key => $value) {
            $new[preg_match('#%\w+%#', $key) ? $key : '%' . $key . '%'] = $value;
        }

        return $new;
    }

    /**
     * @param string $word
     * @param string|null $set
     * @param LocaleInterface|string|null $locale
     * @return bool
     */
    public function has($word, $set = null, $locale = null)
    {
        return $this->findWord($word, $set, $locale) !== null;
    }

    /**
     * @param string $word
     * @param string[] $replacements
     * @return string|null
     */
    public function __invoke($word, $replacements = [])
    {
        return $this->get($word, $replacements);
    }

    /**
     * @return string[]
     */
    public function getSupportedLocales()
    {
        return $this->supportedLocales;
    }

    /**
     * @return LocaleSetInterface[]
     */
    public function getLocaleSets()
    {
        return $this->localeSets;
    }

    /**
     * @return string[]
     */
    public function getTranslationOrder()
    {
        return $this->translationOrder;
    }

    /**
     * @return TranslationSelector[]
     */
    public function getSelectors()
    {
        return $this->selectors;
    }

    /**
     * @return string[]
     */
    public function getDefaultPatterns()
    {
        return $this->defaultPatterns;
    }

    public function all()
    {
        $data = [];
        foreach ($this->localeSets as $name => $localeSet) {
            $data[$name] = $localeSet->all();
        }

        return $data;
    }

    public function onRequest()
    {
        LocalizedString::setLocalizator($this);
    }
}
