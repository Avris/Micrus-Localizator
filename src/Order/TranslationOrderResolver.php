<?php
namespace Avris\Micrus\Localizator\Order;

use Avris\Micrus\Bootstrap\Resolver;
use Avris\Micrus\Controller\Http\RequestInterface;
use Avris\Bag\Bag;

class TranslationOrderResolver implements Resolver
{
    /** @var RequestInterface */
    protected $request;

    /** @var string */
    protected $fallback;

    /** @var Bag */
    protected $supported;

    /**
     * @param RequestInterface $request
     * @param Bag $supported
     * @param string $fallback
     */
    public function __construct(RequestInterface $request, Bag $supported, $fallback = 'en')
    {
        if ($supported->isEmpty()) {
            $supported->set('en', 'English');
        }

        $this->request = $request;
        $this->supported = $supported;
        $this->fallback = $fallback;
    }

    public function resolve()
    {
        $order = new TranslationOrder($this->supported);

        $order->add($this->request->getSession('_locale'), true);

        $routeMatch = $this->request->getRouteMatch();
        if ($routeMatch && isset($routeMatch->getTags()['_locale'])) {
            $order->add($routeMatch->getTags()['_locale']);
        }

        if ($headerLanguages = $this->request->getHeaders()->getLanguage()) {
            $order->addMultiple($headerLanguages->getSorted());
        }

        $order->add($this->fallback, true);

        return $order;
    }
}
