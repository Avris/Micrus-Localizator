<?php
namespace Avris\Micrus\Localizator\Order;

use Avris\Bag\Bag;
use Avris\Bag\Set;
use Avris\Micrus\Localizator\Locale\Locale;
use Avris\Micrus\Localizator\Locale\LocaleInterface;

class TranslationOrder extends Set
{
    /** @var LocaleInterface[] */
    protected $values = [];

    /** @var Bag */
    protected $supportedLocales;

    /** @var Set */
    protected $supportedLanguages;

    /**
     * @param Bag $supported
     * @param LocaleInterface[] $values
     * @param callable|null $callback
     */
    public function __construct(Bag $supported, array $values = [], $callback = null)
    {
        $this->supportedLocales = $supported;
        $this->supportedLanguages = new Set($supported->keys(), function ($v) { return substr($v, 0, 2); });
        parent::__construct($values, $callback);
    }

    public function add($value, $force = false)
    {
        if ($value && !in_array($value, $this->values, true)) {
            $locale = new Locale($value);

            if ($force || $this->supportedLanguages->has($locale->getLanguage())) {
                $this->values[] = $locale;
            }

            if (!$locale->isGeneral()) {
                $this->add($locale->getLanguage());
            }
        }

        return $this;
    }

    public function getList()
    {
        return array_map(function (Locale $locale) {
            return (string) $locale;
        }, $this->values);
    }

    /**
     * @return LocaleInterface|string
     */
    public function main()
    {
        foreach ($this->values as $locale) {
            if ($this->supportedLocales->has((string) $locale)) {
                return $locale;
            }
        }

        return $this->first();
    }
}
