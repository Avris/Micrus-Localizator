<?php
namespace Avris\Micrus\Localizator\Selector;

use Avris\Micrus\MicrusJs\JsFunctionInterface;

class PolishDeclination implements TranslationSelector, JsFunctionInterface
{
    /**
     * @return string
     */
    public function getName()
    {
        return 'polishDeclination';
    }

    /**
     * @param array $replacements
     * @param string[] $versions
     * @return string
     */
    public function select(array $replacements, array $versions)
    {
        list($nominativSing, $nominativ, $genitiv) = $versions;
    
        $count = abs($replacements['%count%']);
        if ($count == 1) {
            return $nominativSing;
        }
        $ones = $count % 10;
        $tens = ($count / 10) % 10;

        return ($tens != 1 && $ones >= 2 && $ones <= 4) ? $nominativ : $genitiv;
    }

    /**
     * @return string
     */
    public function getJsFunction()
    {
        return <<<JS
function (replacements, versions) {
    var count = Math.abs(replacements['%count%']);
    if (count === 1) {
        return versions[0];
    }

    var ones = count % 10;
    var tens = (count / 10) % 10;

    return (tens != 1 && ones >= 2 && ones <= 4) ? versions[1] : versions[2];
}
JS;
    }
}
