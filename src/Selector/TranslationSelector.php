<?php
namespace Avris\Micrus\Localizator\Selector;

interface TranslationSelector
{
    /**
     * @return string
     */
    public function getName();

    /**
     * @param array $replacements
     * @param string[] $versions
     * @return string
     */
    public function select(array $replacements, array $versions);
}
