<?php
namespace Avris\Micrus\Localizator\Selector;

class CountVersion implements TranslationSelector
{
    /**
     * @return string
     */
    public function getName()
    {
        return '';
    }

    /**
     * @param array $replacements
     * @param string[] $versions
     * @return string
     */
    public function select(array $replacements, array $versions)
    {
        $count = isset($replacements['%count%']) ? abs((int) $replacements['%count%']) : 1;

        foreach ($versions as $i => $version) {
            list ($requirement, $translation) = preg_match('#^\{([\d,-]*)\}(.*)#', $version, $matches)
                ? [$matches[1], trim($matches[2])]
                : [$i == 0 ? '1' : '-0,2-', trim($version)];

            if ($this->matches($count, $requirement)) {
                return $translation;
            }
        }

        return '';
    }

    /**
     * @param int $count
     * @param string $requirement
     * @return bool
     */
    protected function matches($count, $requirement)
    {
        foreach (explode(',', $requirement) as $range) {
            list($min, $max) = explode('-', $range) + [1 => null];

            if ($this->rangeMatches($count, $min, $max)) {
                return true;
            }
        }

        return false;
    }

    protected function rangeMatches($count, $min, $max)
    {
        if (!isset($max)) {
            if ($min === '') {
                return true;
            }

            return $count == $min;
        }

        if ($min !== '' && $max === '') {
            return $count >= $min;
        }

        if ($min === '' && $max !== '') {
            return $count <= $max;
        }

        return $count >= $min && $count <= $max;
    }
}
