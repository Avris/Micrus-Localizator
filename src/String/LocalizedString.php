<?php
namespace Avris\Micrus\Localizator\String;

use Avris\Micrus\Localizator\Locale\LocaleInterface;
use Avris\Micrus\Localizator\Localizator;

class LocalizedString implements \JsonSerializable
{
    /** @var Localizator */
    protected static $localizator;

    /** @var string */
    protected $word;

    /** @var array */
    protected $replacements;

    /** @var string|null */
    protected $set;

    /** @var LocaleInterface|string|null */
    protected $locale;


    /**
     * @param string $word
     * @param string[] $replacements
     * @param string|null $set
     * @param LocaleInterface|string|null $locale
     */
    public function __construct($word, $replacements = [], $set = null, $locale = null)
    {
        $this->word = (string) $word;
        $this->replacements = $replacements;
        $this->set = $set;
        $this->locale = $locale;
    }

    /**
     * @param Localizator $localizator
     */
    public static function setLocalizator(Localizator $localizator = null)
    {
        static::$localizator = $localizator;
    }

    /**
     * @return Localizator
     */
    public static function getLocalizator()
    {
        return self::$localizator;
    }

    /**
     * @return string
     */
    public function getLocalized()
    {
        return static::$localizator
            ? static::$localizator->get($this->word, $this->replacements, $this->set, $this->locale)
            : str_replace(array_keys($this->replacements), array_values($this->replacements), $this->word);
    }

    public function exists()
    {
        return static::$localizator
            ? static::$localizator->has($this->word, $this->set, $this->locale)
            : true;
    }

    public function __toString()
    {
        return $this->getLocalized();
    }

    public function jsonSerialize()
    {
        return $this->getLocalized();
    }
}
