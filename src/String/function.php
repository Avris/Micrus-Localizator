<?php
use Avris\Micrus\Localizator\String\LocalizedString;

function l($word, $replacements = [], $set = null, $locale = null)
{
    return new LocalizedString($word, $replacements, $set, $locale);
}
