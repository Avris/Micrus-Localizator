<?php
namespace Avris\Micrus\Localizator\Locale;

use Avris\Bag\Bag;

abstract class FileLocaleSet implements LocaleSetInterface
{
    /** @var string */
    protected $name;

    /** @var string */
    protected $dir;

    /** @var LocaleInterface[] */
    protected $locales;

    /** @var LocaleInterface */
    protected $fallback;

    /**
     * @param string $name
     * @param string $dir
     * @param string $fallback
     */
    public function __construct($name, $dir, $fallback = 'en')
    {
        $this->name = $name;
        $this->dir = $dir;
        $this->locales = $this->fetchLocaleList();
        $this->fallback = $this->findFallback($fallback);
    }

    /**
     * @return Bag|LocaleInterface[]
     */
    protected function fetchLocaleList()
    {
        $localeList = new Bag();
        foreach (glob($this->dir . '/*.' . $this->getExtension()) as $locale) {
            $locale = explode('/', str_replace('\\', '/', $locale));
            $locale = substr(end($locale), 0, -4);
            $localeList[$locale] = new FileLocale($this, $locale);
        }

        return $localeList;
    }

    /**
     * @param string $code
     * @return LocaleInterface
     */
    protected function findFallback($code)
    {
        if ($this->locales->offsetExists($code)) {
            return $this->locales->get($code);
        }

        if (strpos($code, '_') !== false) {
            $parent = substr($code, 0, 2);
            if ($this->locales->has($parent)) {
                return $this->locales->get($parent);
            }
        }

        return new FileLocale($this, $code);
    }

    /**
     * @param string $code
     * @param $word
     * @return string|null
     */
    public function get($code, $word)
    {
        return $this->locales->has($code)
            ? $this->locales->get($code)->get($word)
            : null;
    }

    /**
     * @param LocaleInterface $locale
     * @return Bag
     */
    public function loadDataForLocale(LocaleInterface $locale)
    {
        $filename = $this->dir . '/' . $locale . '.' . $this->getExtension();

        return new Bag(file_exists($filename) ? $this->loadFile($filename) : []);
    }

    /**
     * @param string $filename
     * @return array
     */
    abstract protected function loadFile($filename);

    /**
     * @return string
     */
    abstract protected function getExtension();

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return Bag|LocaleInterface[]
     */
    public function all()
    {
        return $this->locales;
    }

    /**
     * @return LocaleInterface
     */
    public function getFallback()
    {
        return $this->fallback;
    }
}
