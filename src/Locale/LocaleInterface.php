<?php
namespace Avris\Micrus\Localizator\Locale;

interface LocaleInterface
{
    /**
     * @return string|null
     */
    public function getCountry();

    /**
     * @return string
     */
    public function getLanguage();

    /**
     * @return bool
     */
    public function isGeneral();

    /**
     * @return string
     */
    public function __toString();

    /**
     * @param string $word
     * @return string|null
     */
    public function get($word);

    /**
     * @return array
     */
    public function all();
}