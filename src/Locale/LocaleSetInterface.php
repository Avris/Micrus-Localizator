<?php
namespace Avris\Micrus\Localizator\Locale;

interface LocaleSetInterface
{
    /**
     * @param string $code
     * @param $word
     * @return string|null
     */
    public function get($code, $word);

    /**
     * @return string
     */
    public function getName();

    /**
     * @return LocaleInterface[]
     */
    public function all();

    /**
     * @return LocaleInterface
     * */
    public function getFallback();
}
