<?php
namespace Avris\Micrus\Localizator\Locale;

class Locale
{
    /** @var string */
    protected $language;

    /** @var string */
    protected $country;

    /**
     * @param string $code format: xx or xx_YY
     */
    public function __construct($code)
    {
        $code = explode('_', $code);
        $this->language = $code[0];
        $this->country = isset($code[1]) ? $code[1] : null;
    }

    public function __toString()
    {
        return $this->country ? $this->language . '_' . $this->country : $this->language;
    }

    /**
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @return string|null
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @return bool
     */
    public function isGeneral()
    {
        return $this->country === null;
    }

    public function guessFlag()
    {
        return $this->country ?: strtoupper($this->language);
    }
}
