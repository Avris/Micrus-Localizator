<?php
namespace Avris\Micrus\Localizator\Locale;

use Avris\Bag\Bag;

class FileLocale extends Locale implements LocaleInterface
{
    /** @var FileLocaleSet */
    protected $set;

    /** @var Bag */
    protected $data;

    /**
     * @param FileLocaleSet $set
     * @param string $code format: xx or xx_YY
     */
    public function __construct(FileLocaleSet $set, $code)
    {
        $this->set = $set;
        parent::__construct($code);
    }

    /**
     * @param string $word
     * @return string|null
     */
    public function get($word)
    {
        return $this->getData()->getDeep($word);
    }

    /**
     * @return array
     */
    public function all()
    {
        return $this->getData()->all();
    }

    /**
     * @return Bag
     */
    protected function getData()
    {
        return $this->data ?: $this->data = $this->set->loadDataForLocale($this);
    }

    public function __sleep()
    {
        return array_diff(array_keys(get_object_vars($this)), ['set']);
    }
}
