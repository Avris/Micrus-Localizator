<?php
namespace Avris\Micrus\Localizator\Locale;

class PhpLocaleSet extends FileLocaleSet
{
    /**
     * @param string $filename
     * @return array
     */
    protected function loadFile($filename)
    {
        return require $filename;
    }

    /**
     * @return string
     */
    protected function getExtension()
    {
        return 'php';
    }
}
