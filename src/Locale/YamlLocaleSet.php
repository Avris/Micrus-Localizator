<?php
namespace Avris\Micrus\Localizator\Locale;

class YamlLocaleSet extends FileLocaleSet
{
    /**
     * @param string $filename
     * @return array
     */
    protected function loadFile($filename)
    {
        return \Spyc::YAMLLoad($filename);
    }

    /**
     * @return string
     */
    protected function getExtension()
    {
        return 'yml';
    }
}
