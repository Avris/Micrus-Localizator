<?php
namespace Avris\Micrus\Localizator;

use Avris\Micrus\Localizator\Order\TranslationOrder;
use Avris\Micrus\View\AppVarsInterface;

class LocalizatorAppVars implements AppVarsInterface
{
    /** @var Localizator */
    protected $localizator;

    /** @var TranslationOrder */
    protected $translationOrder;

    public function __construct(Localizator $localizator, TranslationOrder $translationOrder)
    {
        $this->localizator = $localizator;
        $this->translationOrder = $translationOrder;
    }

    public function buildAppVars()
    {
        return [
            'locales' => $this->localizator->getSupportedLocales(),
            'locale' => $this->translationOrder->main(),
        ];
    }
}
