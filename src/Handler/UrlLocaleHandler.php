<?php
namespace Avris\Micrus\Localizator\Handler;

use Avris\Micrus\Bootstrap\ContainerInterface;
use Avris\Micrus\Controller\Routing\Event\AddRouteEvent;
use Avris\Micrus\Controller\Routing\Event\GenerateRouteEvent;
use Avris\Micrus\Model\AutomatcherSpecialTags;

class UrlLocaleHandler implements AutomatcherSpecialTags
{
    /** @var ContainerInterface */
    protected $container;

    /** @var string */
    protected $currentLocale;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function onAddRoute(AddRouteEvent $event)
    {
        $route = $event->getRoute();
        if ($route->getOption('immutable')) {
            return;
        }
        $route->setPattern('/{_locale}' . $route->getPattern());
        $route->setDefaults($route->getDefaults() + ['_locale' => '']);
    }

    public function onGenerateRoute(GenerateRouteEvent $event)
    {
        $tagNames = $event->getRoute()->getTagNames();
        $params = $event->getParams();

        if (in_array('_locale', $tagNames) && !array_key_exists('_locale', $params)) {
            $params['_locale'] = $this->currentLocale
                ?: $this->currentLocale = (string) $this->container->get('translationOrder')->first();
            $event->setParams($params);
        }
    }

    public function getSpecialTags()
    {
        return ['_locale' => null];
    }
}
