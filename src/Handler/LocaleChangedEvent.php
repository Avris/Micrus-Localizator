<?php
namespace Avris\Micrus\Localizator\Handler;

use Avris\Micrus\Bootstrap\Event;

class LocaleChangedEvent extends Event
{
    /** @var string */
    private $oldLocale;

    /** @var string */
    private $newLocale;

    /** @var bool */
    private $shouldChange = true;

    /**
     * @param string $oldLocale
     * @param string $newLocale
     */
    public function __construct($oldLocale, $newLocale)
    {
        $this->oldLocale = $oldLocale;
        $this->newLocale = $newLocale;
    }

    /**
     * @return string
     */
    public function getOldLocale()
    {
        return $this->oldLocale;
    }

    /**
     * @return string
     */
    public function getNewLocale()
    {
        return $this->newLocale;
    }

    /**
     * @param string $newLocale
     * @return $this
     */
    public function setNewLocale($newLocale)
    {
        $this->newLocale = $newLocale;

        return $this;
    }

    public function shouldChange()
    {
        return $this->shouldChange;
    }

    /**
     * @param bool $shouldChange
     * @return $this
     */
    public function setShouldChange($shouldChange)
    {
        $this->shouldChange = $shouldChange;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'localeChanged';
    }
}