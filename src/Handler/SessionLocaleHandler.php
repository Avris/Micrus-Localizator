<?php
namespace Avris\Micrus\Localizator\Handler;

use Avris\Micrus\Bootstrap\EventDispatcherInterface;
use Avris\Micrus\Controller\Http\RedirectResponse;
use Avris\Micrus\Controller\Http\RequestInterface;
use Avris\Micrus\Controller\Routing\RoutingExtension;
use Avris\Micrus\Controller\Routing\Service\RouterInterface;
use Avris\Micrus\Exception\NotFoundException;
use Avris\Micrus\Bootstrap\ContainerInterface;
use Avris\Micrus\Localizator\Localizator;

class SessionLocaleHandler implements RoutingExtension
{
    /** @var Localizator */
    protected $localizator;

    /** @var EventDispatcherInterface */
    protected $dispatcher;

    /**
     * @param Localizator $localizator
     */
    public function __construct(Localizator $localizator, EventDispatcherInterface $dispatcher)
    {
        $this->localizator = $localizator;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param ContainerInterface $container
     * @param string $locale
     * @return RedirectResponse
     * @throws NotFoundException
     */
    public function changeLocaleAction(ContainerInterface $container, $locale)
    {
        if (!in_array($locale, array_keys($this->localizator->getSupportedLocales()))) {
            throw new NotFoundException;
        }

        /** @var RequestInterface $request */
        $request = $container->get('request');

        $event = new LocaleChangedEvent($request->getSession()->get('_locale'), $locale);
        $this->dispatcher->trigger($event);
        if ($event->shouldChange()) {
            $request->getSession()->set('_locale', $event->getNewLocale());
        }

        return new RedirectResponse(
            $request->getHeaders('referer') ?: $container->get('router')->getUrl(RouterInterface::DEFAULT_ROUTE)
        );
    }

    public function getRouting()
    {
        return [
            'changeLocale' => '/locale/{<[a-z]{2}(?:_[A-Z]{2})?>:locale} -> @localizatorHandler/changeLocale',
        ];
    }
}
