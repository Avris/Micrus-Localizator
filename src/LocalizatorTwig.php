<?php
namespace Avris\Micrus\Localizator;

class LocalizatorTwig extends \Twig_Extension
{
    /** @var Localizator */
    protected $localizator;

    public function __construct(Localizator $localizator)
    {
        $this->localizator = $localizator;
    }

    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('l', function ($word, $replacements = [], $set = null, $locale = null) {
                return $this->localizator->get($word, $replacements, $set, $locale);
            }, ['is_safe' => ['html']])
        ];
    }
}
