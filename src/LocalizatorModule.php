<?php
namespace Avris\Micrus\Localizator;

use Avris\Micrus\Localizator\Handler\SessionLocaleHandler;
use Avris\Micrus\Localizator\Handler\UrlLocaleHandler;
use Avris\Micrus\Localizator\Locale\YamlLocaleSet;
use Avris\Micrus\Bootstrap\Module;
use Avris\Micrus\Localizator\Order\TranslationOrderResolver;
use Avris\Micrus\Localizator\Selector\CountVersion;
use Avris\Micrus\Localizator\Selector\PolishDeclination;

class LocalizatorModule implements Module
{
    public function extendConfig($env, $rootDir)
    {
        return [
            'services' => [
                'localizator' => [
                    'class' => Localizator::class,
                    'params' => [
                        '#localeSet',
                        '@translationOrder',
                        '@config.?locales',
                        '#translationSelector',
                        [
                            '^entity\.(.*)\.singular$',
                            '^entity\.(.*)\.plural$',
                            '^entity\..*\.fields.(.*)$',
                            '^entity\..*\.sections.(.*)$',
                        ]
                    ],
                    'events' => ['request:900'],
                ],
                'appLocaleSet' => [
                    'class' => YamlLocaleSet::class,
                    'params' => ['app', '{@rootDir}/app/Locale', '@config.?fallbackLocale'],
                    'tags' => ['localeSet'],
                ],
                'localizatorHandler' => [
                    'class' => SessionLocaleHandler::class,
                    'params' => ['@localizator', '@dispatcher'],
                    'tags' => ['routingExtension'],
                ],
//                'localizatorHandler' => [
//                    'class' => UrlLocaleHandler::class,
//                    'params' => ['@'],
//                    'events' => ['addRoute', 'generateRoute'],
//                    'tags' => ['automatcherSpecialTags'],
//                ],
                'translationOrder' => [
                    'class' => TranslationOrderResolver::class,
                    'params' => ['@request', '@config.?locales', '@config.?fallbackLocale'],
                ],
                'countVersion' => [
                    'class' => CountVersion::class,
                    'tags' => ['translationSelector'],
                ],
                'localizeTwig' => [
                    'class' => LocalizatorTwig::class,
                    'params' => ['@localizator'],
                    'tags' => ['twigExtension'],
                ],
                'localizeAppVars' => [
                    'class' => LocalizatorAppVars::class,
                    'params' => ['@localizator', '@translationOrder'],
                    'tags' => ['appVars'],
                ],
//                'polishDeclination' => [
//                    'class' => PolishDeclination::class,
//                    'tags' => ['translationSelector'],
//                ],
            ]
        ];
    }
}
